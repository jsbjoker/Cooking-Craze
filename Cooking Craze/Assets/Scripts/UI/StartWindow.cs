using CookingPrototype.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CookingPrototype.UI {
	public class StartWindow : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI _targetGoalText;
		[SerializeField] private Button _startButton;
		[SerializeField] private Button _exitButton;
		

		private void Awake() 
		{
			_startButton.onClick.AddListener(StartGame);
			_exitButton.onClick.AddListener(StartGame);
		}

		private void StartGame() => GameplayController.Instance.StartGame();

		public void Show() 
		{
			_targetGoalText.text = GameplayController.Instance.OrdersTarget.ToString();
			gameObject.SetActive(true);
		}
		
		public void Hide() 
		{
			gameObject.SetActive(false);
		}

		private void OnDestroy() 
		{
			_startButton.onClick.RemoveListener(StartGame);
			_exitButton.onClick.RemoveListener(StartGame);
		}
	}
}