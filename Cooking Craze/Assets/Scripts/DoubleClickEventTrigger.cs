using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace DefaultNamespace {
	public class DoubleClickEventTrigger : MonoBehaviour, IPointerUpHandler {
		[SerializeField] private float _doubleClickDelay = 0.2f;
		[SerializeField] private CustomUnityEvent _doubleClickEvent;

		private float _lastTimeClick;
		
		public void OnPointerUp(PointerEventData eventData) {
			var deltaBetweenClicks = Time.time - _lastTimeClick;
			if ( deltaBetweenClicks <= _doubleClickDelay ) {
				_doubleClickEvent?.Invoke();
			}
			_lastTimeClick = Time.time;
		}
	}
	
	[Serializable]
	public class CustomUnityEvent : UnityEvent {}
}